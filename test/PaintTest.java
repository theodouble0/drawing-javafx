import drawing.PaintApplication;
import drawing.shapes.IShape;
import drawing.shapes.ShapeAdapter;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.util.Iterator;

import static org.junit.Assert.*;
public class PaintTest extends ApplicationTest {

    PaintApplication app;

    @Override
    public void start(Stage stage) {
        try {
            app = new PaintApplication();
            app.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_draw_circle_programmatically() {
        interact(() -> {
                    app.getDrawingPane().addShape(new ShapeAdapter(new Ellipse(20, 20, 30, 30)));
                });
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof IShape);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_circle() {
        // given:
        clickOn("Circle");
        moveBy(60,60);

        // when:
        drag().dropBy(30,30);
        //press(MouseButton.PRIMARY); moveBy(30,30); release(MouseButton.PRIMARY);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof IShape);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_rectangle() {
        // given:
        clickOn("Rectangle");
        moveBy(0,60);

        // when:
        drag().dropBy(70,40);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof IShape);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_triangle() {
        // given:
        clickOn("Triangle");
        moveBy(0,60);

        // when:
        drag().dropBy(70,40);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof IShape);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_clear() {
        // given:
        clickOn("Rectangle");
        moveBy(30,60).drag().dropBy(70,40);
        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);

        // when:
        clickOn("Clear");

        // then:
        assertFalse(app.getDrawingPane().iterator().hasNext());
    }

    @Test
    public void statut_bar() {
        // given:
        clickOn("Rectangle");
        moveBy(30,60).drag().dropBy(70,40);
        assertEquals(app.getDrawingPane().getNbShapes(), 1);

        clickOn("Circle");
        moveBy(30,60).drag().dropBy(70,40);
        assertEquals(app.getDrawingPane().getNbShapes(), 2);

        clickOn("Triangle");
        moveBy(30,60).drag().dropBy(70,40);
        assertEquals(app.getDrawingPane().getNbShapes(), 3);

        // when:
        clickOn("Clear");
        assertEquals(app.getDrawingPane().getNbShapes(), 0);
    }

    @Test
    public void group() {
                clickOn("Rectangle");
        moveBy(45,75).drag().dropBy(95,55);
        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);
        press(KeyCode.SHIFT).clickOn();
        press(KeyCode.SHIFT).moveBy(-20,-100).clickOn();
        release(KeyCode.SHIFT);
        assertEquals(2, app.getDrawingPane().getNbShapes());
        clickOn("Group");
        int selSize = 0;
        for(IShape s: app.getDrawingPane().getSelection())
            selSize++;
        moveBy(100,100).clickOn();
        assertEquals(1, app.getDrawingPane().getNbShapes());
        clickOn("Clear");


    }

    @Test
    public void degroup() {
        clickOn("Rectangle");
        moveBy(45,75).drag().dropBy(95,55);
        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);
        press(KeyCode.SHIFT).clickOn();
        press(KeyCode.SHIFT).moveBy(-20,-100).clickOn();
        release(KeyCode.SHIFT);
        clickOn("Group");
        assertEquals(1, app.getDrawingPane().getNbShapes());
        moveBy(0,100).clickOn();
        moveBy(-300,0).clickOn();
        clickOn("Degroup");
        assertEquals(2, app.getDrawingPane().getNbShapes());
        clickOn("Clear");
    }


    @Test
    public void undo_redo_shape() {
        clickOn("Rectangle");
        moveBy(45,75).drag().dropBy(95,55);
        assertEquals(1, app.getDrawingPane().getNbShapes());
        clickOn("Undo");
        assertEquals(0, app.getDrawingPane().getNbShapes());
        clickOn("Redo");
        assertEquals(1, app.getDrawingPane().getNbShapes());
    }


    @Test
    public void undo_redo_clear() {
        clickOn("Rectangle");
        moveBy(30,60).drag().dropBy(70,40);
        clickOn("Circle");
        moveBy(30,60).drag().dropBy(70,40);
        clickOn("Triangle");
        moveBy(30,60).drag().dropBy(70,40);

        assertEquals(app.getDrawingPane().getNbShapes(), 3);
        clickOn("Clear");
        assertEquals(app.getDrawingPane().getNbShapes(), 0);
        clickOn("Undo");
        assertEquals(app.getDrawingPane().getNbShapes(), 3);
        clickOn("Redo");
        assertEquals(app.getDrawingPane().getNbShapes(), 0);
    }

    @Test
    public void undo_redo_delete() {
        clickOn("Rectangle");
        moveBy(45,75).drag().dropBy(95,55);
        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);
        press(KeyCode.SHIFT).clickOn();
        press(KeyCode.SHIFT).moveBy(-20,-100).clickOn();
        release(KeyCode.SHIFT);

        assertEquals(app.getDrawingPane().getNbShapes(), 2);
        clickOn("Delete");
        assertEquals(app.getDrawingPane().getNbShapes(), 0);
        clickOn("Undo");
        assertEquals(app.getDrawingPane().getNbShapes(), 2);
        clickOn("Redo");
        assertEquals(app.getDrawingPane().getNbShapes(), 0);
    }

    @Test
    public void undo_redo_group() {
        clickOn("Rectangle");
        moveBy(45,75).drag().dropBy(95,55);
        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);
        press(KeyCode.SHIFT).clickOn();
        press(KeyCode.SHIFT).moveBy(-20,-100).clickOn();
        release(KeyCode.SHIFT);

        assertEquals(app.getDrawingPane().getNbShapes(), 2);
        clickOn("Group");
        assertEquals(app.getDrawingPane().getNbShapes(), 1);
        clickOn("Undo");
        assertEquals(app.getDrawingPane().getNbShapes(), 2);
        clickOn("Redo");
        assertEquals(app.getDrawingPane().getNbShapes(), 1);
    }

    @Test
    public void undo_redo_degroup() {
        clickOn("Rectangle");
        moveBy(45,75).drag().dropBy(95,55);
        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);
        press(KeyCode.SHIFT).clickOn();
        press(KeyCode.SHIFT).moveBy(-20,-100).clickOn();
        release(KeyCode.SHIFT);

        assertEquals(app.getDrawingPane().getNbShapes(), 2);
        clickOn("Group");
        assertEquals(app.getDrawingPane().getNbShapes(), 1);
        moveBy(0,100).clickOn();
        moveBy(-300,0).clickOn();
        clickOn("Degroup");
        assertEquals(app.getDrawingPane().getNbShapes(), 2);
        clickOn("Undo");
        assertEquals(app.getDrawingPane().getNbShapes(), 1);
        clickOn("Redo");
        assertEquals(app.getDrawingPane().getNbShapes(), 2);
    }
}