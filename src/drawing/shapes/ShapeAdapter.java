package drawing.shapes;

import com.sun.org.omg.CORBA.InitializerSeqHelper;
import drawing.shapes.IShape;
import javafx.collections.ObservableList;
import javafx.scene.layout.Pane;
import javafx.scene.shape.*;

public class ShapeAdapter implements IShape {

    private Shape shape;

    public ShapeAdapter(Shape shape) {
        this.shape = shape;
    }

    @Override
    public boolean isSelected() {
        return shape.getStyleClass().contains("selected");
    }

    @Override
    public void setSelected(boolean selected) {
        if (selected)
            shape.getStyleClass().add("selected");
        else
            shape.getStyleClass().remove("selected");
    }

    @Override
    public boolean isOn(double x, double y) {
        return shape.getBoundsInParent().contains(x, y);
    }

    @Override
    public void offset(double x, double y) {
        shape.setTranslateX(shape.getTranslateX() + x);
        shape.setTranslateY(shape.getTranslateY() + y);
    }

    @Override
    public void addShapeToPane(Pane pane) {
        pane.getChildren().add(shape);
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        pane.getChildren().remove(shape);
    }

    @Override
    public IShape clone() {
        Shape cp = null;
        if (this.shape instanceof Rectangle) {
            double h = ((Rectangle) shape).getHeight();
            double w = ((Rectangle) shape).getWidth();
            double x = ((Rectangle) shape).getX();
            double y = ((Rectangle) shape).getY();
            cp = new Rectangle(x, y, w, h);
            cp.getStyleClass().add("rectangle");
        } else if (this.shape instanceof Polygon) {
            ObservableList<Double> points = ((Polygon) shape).getPoints();
            cp = new Polygon(points.get(0),points.get(1),points.get(2),points.get(3),points.get(4),points.get(5));
            cp.getStyleClass().add("triangle");
        } else if (this.shape instanceof Ellipse){
            double h = ((Ellipse) shape).getRadiusX();
            cp = new Ellipse(((Ellipse) shape).getCenterX(), ((Ellipse) shape).getCenterY(), ((Ellipse) shape).getRadiusX(), ((Ellipse) shape).getRadiusY());
            cp.getStyleClass().add("ellipse");
        }
        return new ShapeAdapter(cp);
    }
}
