package drawing.shapes;

import drawing.ui.DrawingPane;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import org.junit.Test;
import org.omg.CosNaming.IstringHelper;

public class TextDecorator implements IShape {

    private IShape shape;
    Text l;

    public TextDecorator(DrawingPane drawingPane, IShape shape, Text l){
        this.shape = shape;
        //l = new Text("test");
        this.l = l;
        l.setX(100);
        l.setY(100);
        drawingPane.getChildren().add(l);
    }

    @Override
    public boolean isSelected() {
        return shape.isSelected();
    }

    @Override
    public void setSelected(boolean selected) {
        shape.setSelected(selected);
    }

    @Override
    public boolean isOn(double x, double y) {
        return shape.isOn(x, y);
    }

    @Override
    public void offset(double x, double y) {
        l.setX(l.getX() + x);
        l.setY(l.getY() + y);
        shape.offset(x, y);
    }

    @Override
    public void addShapeToPane(Pane pane) {
        shape.addShapeToPane(pane);
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        shape.removeShapeFromPane(pane);
    }

    @Override
    public IShape clone() {
        return shape.clone();
    }
}
