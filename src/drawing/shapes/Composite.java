package drawing.shapes;

import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.Iterator;

public class Composite implements IShape {

    ArrayList<IShape> shapes;

    public Composite(){
        this.shapes = new ArrayList<>();
    }
    public void add(IShape shape){
        shapes.add(shape);
    }
    public void remove(IShape shape){
        shapes.remove(shape);
    }
    public ArrayList<IShape> getShapes(){
        return shapes;
    }

    @Override
    public boolean isSelected() {
        return shapes.get(0).isSelected();
    }

    @Override
    public void setSelected(boolean selected) {
        shapes.forEach((s) ->{
            s.setSelected(selected);
        });
    }

    @Override
    public boolean isOn(double x, double y) {
        for(IShape s : shapes){
            if(s.isOn(x, y))
                return true;
        }
        return false;
    }

    @Override
    public void offset(double x, double y) {
        shapes.forEach((s) -> s.offset(x, y));
    }

    @Override
    public void addShapeToPane(Pane pane) {
        shapes.forEach((s) -> { s.addShapeToPane(pane);});
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        shapes.forEach((s) -> { s.removeShapeFromPane(pane);});
    }

    @Override
    public IShape clone() {
        Composite cp = new Composite();
        shapes.forEach((s) -> {
            cp.add(s.clone());
        });
        return cp;
    }
}
