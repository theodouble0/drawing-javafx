package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.Iterator;

public class DeleteCommand implements ICommand {
    private DrawingPane drawingPane;
    private ArrayList<IShape> shapesHistory;

    public DeleteCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
        shapesHistory = new ArrayList<>();
    }

    @Override
    public void execute() throws Exception{
        shapesHistory.clear();
        for(IShape shape: drawingPane.getSelection()) {
            shapesHistory.add(shape);
            drawingPane.removeShape(shape);
        }
        if(shapesHistory.size() == 0)
            throw new Exception("aucune forme sélectionné a efface");
        drawingPane.getSelection().clearSelection();
    }

    @Override
    public void undo() {
        shapesHistory.forEach((s) -> {
            drawingPane.addShape(s);
        });
    }

    @Override
    public void redo() {
        shapesHistory.forEach((s) -> {
            drawingPane.removeShape(s);
        });
    }
}
