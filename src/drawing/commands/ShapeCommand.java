package drawing.commands;

import drawing.shapes.IShape;
import drawing.shapes.ShapeAdapter;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;

public class ShapeCommand implements ICommand{
    private DrawingPane drawingPane;
    private IShape shape;

    public ShapeCommand(DrawingPane drawingPane, IShape shape){
        this.drawingPane = drawingPane;
        this.shape = shape;
    }

    @Override
    public void execute() {
        this.drawingPane.addShape(shape);
    }

    @Override
    public void undo() {
        this.drawingPane.removeShape(shape);
    }

    @Override
    public void redo() {
        execute();
    }
}
