package drawing.commands;

import drawing.shapes.IShape;

import java.util.ArrayList;

public class CommandHistory {
    private ArrayList<ICommand> history;
    private int index;
    public CommandHistory(){
        history = new ArrayList<>();
    }

    public void exec(ICommand command) throws Exception{
        try{
            command.execute();
            history.add(command);
            index = history.size()-1;
        }catch(Exception e){
            throw e;
        }
    }

    public void undo() throws Exception{
        if (!history.isEmpty() && index >=0) {
            this.history.get(index).undo();
            index -= 1;
        }else
            throw new Exception("rien a undo");
    }

    public void redo() throws Exception{
        if (!history.isEmpty() && index < history.size()-1){
            index += 1;
            this.history.get(index).redo();
        } else
            throw new Exception("rien a redo");
    }
}
