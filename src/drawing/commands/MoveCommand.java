package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;

public class MoveCommand implements ICommand {

    private DrawingPane drawingPane;
    private IShape shape;
    private double offsetX;
    private double offsetY;

    private ArrayList<IShape> select;

    public MoveCommand(DrawingPane drawingPane, double offsetX, double offsetY){
        this.drawingPane = drawingPane;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.select = new ArrayList<>();
    }

    @Override
    public void execute() {
        for( IShape s : this.drawingPane.getSelection())
            this.select.add(s);
    }

    @Override
    public void undo() {
        for(IShape shape : select)
            shape.offset(-offsetX, -offsetY);
    }

    @Override
    public void redo() {
        for(IShape shape : select)
            shape.offset(offsetX, offsetY);
    }
}
