package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.Iterator;

public class ClearCommand implements ICommand {
    private DrawingPane drawingPane;
    private ArrayList<IShape> shapesHistory;

    public ClearCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
        this.shapesHistory = new ArrayList<>();
    }

    @Override
    public void execute() throws Exception{
        shapesHistory.clear();
        Iterator<IShape> shapeIterator = this.drawingPane.iterator();
        while (shapeIterator.hasNext()){
            shapesHistory.add(shapeIterator.next());
        }
        if(shapesHistory.size() == 0)
            throw new Exception("aucune forme a efface");
        this.drawingPane.clear();
        this.drawingPane.getSelection().clearSelection();
    }

    @Override
    public void undo() {
        shapesHistory.forEach((s) -> {
            drawingPane.addShape(s);
        });
    }

    @Override
    public void redo() {
        try {
            execute();
        }catch (Exception e){

        }
    }
}
