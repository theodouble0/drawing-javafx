package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;

import java.util.ArrayList;

public class CloneCommand implements ICommand{

    private DrawingPane drawingPane;
    private ArrayList<IShape> shapesHistory;

    public CloneCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
        this.shapesHistory = new ArrayList<>();
    }

    @Override
    public void execute() throws Exception{
        boolean clone = false;
        for(IShape s : drawingPane.getSelection()){
            shapesHistory.add(s.clone());
            clone = true;
        }
        if(!clone)
            throw new Exception("aucune forme sélectionné a cloner");
        shapesHistory.forEach((s) -> {
            drawingPane.addShape(s);
        });
    }

    @Override
    public void undo() {
        shapesHistory.forEach((s) -> {
            drawingPane.removeShape(s);
        });
    }

    @Override
    public void redo() {
        shapesHistory.forEach((s) -> {
            drawingPane.addShape(s);
        });
    }
}
