package drawing.commands;

import drawing.shapes.Composite;
import drawing.shapes.IShape;
import drawing.ui.DrawingPane;

import java.util.ArrayList;

public class DegroupCommand implements ICommand{
    private DrawingPane drawingPane;
    private ArrayList<Composite> select;

    public DegroupCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
        this.select = new ArrayList<>();
    }

    @Override
    public void execute() throws Exception{
        boolean composite = false;
        this.select.clear();
        for(IShape s : this.drawingPane.getSelection()){
            if(s instanceof Composite) {
                composite = true;
                this.drawingPane.removeShape(s);
                for(IShape sp : ((Composite) s).getShapes()){
                    this.drawingPane.addShape(sp);
                }
                this.select.add((Composite) s);
            }
        }
        if(!composite)
            throw new Exception("aucun groupe sélectionné");
    }

    @Override
    public void undo() {
        Composite cp = new Composite();
        this.select.forEach((s) -> {
            s.getShapes().forEach((c) -> {
               this.drawingPane.removeShape(c);
            });
            this.drawingPane.addShape(s);
        });
    }

    @Override
    public void redo() {
        for(IShape s : this.select){
            if(s instanceof Composite) {
                this.drawingPane.removeShape(s);
                for(IShape sp : ((Composite) s).getShapes()){
                    this.drawingPane.addShape(sp);
                }
            }
        }
    }
}
