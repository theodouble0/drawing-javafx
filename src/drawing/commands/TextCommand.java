package drawing.commands;

import drawing.shapes.IShape;
import drawing.shapes.TextDecorator;
import drawing.ui.DrawingPane;
import javafx.scene.text.Text;

public class TextCommand implements ICommand{
    private DrawingPane drawingPane;
    private IShape shape;
    private TextDecorator deco;
    private Text l;

    public TextCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() throws Exception {
        int size = 0;
        for(IShape s : drawingPane.getSelection()){
            size += 1;
        }
        if(size >1)
            throw new Exception("Trop de formes sélectionné");
        if(size == 0)
            throw new Exception("aucune forme sélectionnné");

        drawingPane.getSelection().forEach((s) -> {
            shape = s;
            drawingPane.removeShape(s);
            l = new Text("test");
            deco = new TextDecorator(drawingPane, s, l);
            drawingPane.addShape(deco);
        });
    }

    @Override
    public void undo() {
        drawingPane.removeShape(deco);
        drawingPane.getChildren().remove(l);
        drawingPane.addShape(shape);
    }

    @Override
    public void redo() {
        drawingPane.removeShape(shape);
        drawingPane.getChildren().add(l);
        drawingPane.addShape(deco);
    }
}
