package drawing.commands;

import drawing.shapes.Composite;
import drawing.shapes.IShape;
import drawing.ui.DrawingPane;

import java.util.ArrayList;

public class GroupCommand implements ICommand {
    private DrawingPane drawingPane;
    private ArrayList<Composite> select;

    public GroupCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
        this.select = new ArrayList<>();
    }

    @Override
    public void execute() throws Exception{
        int nbSelected = 0;
        for(IShape s : this.drawingPane.getSelection()){
            nbSelected++;
        }
        if(nbSelected <2)
            throw new Exception("pas assez de forme sélectionné pour grouper");

        if(nbSelected >= 2) {
            Composite cp = new Composite();
            this.drawingPane.getSelection().forEach((s) -> {
                this.drawingPane.removeShape(s);
                cp.add(s);
            });
            this.select.add(cp);
            this.drawingPane.addShape(cp);
        }
    }

    @Override
    public void undo() {
        for(IShape s : this.select){
            if(s instanceof Composite) {
                this.drawingPane.removeShape(s);
                for(IShape sp : ((Composite) s).getShapes()){
                    this.drawingPane.addShape(sp);
                }
            }
        }
    }

    @Override
    public void redo() {
        Composite cp = new Composite();
        this.select.forEach((oldCp) -> {
            oldCp.getShapes().forEach((s) -> {
                this.drawingPane.removeShape(s);
                cp.add(s);
            });
        });
        this.drawingPane.addShape(cp);
    }
}
