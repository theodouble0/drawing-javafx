package drawing.handlers;

import drawing.commands.ClearCommand;
import drawing.commands.CommandHistory;
import drawing.commands.GroupCommand;
import drawing.commands.ICommand;
import drawing.shapes.Composite;
import drawing.shapes.IShape;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.ArrayList;

public class GroupButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;
    CommandHistory command;
    ICommand group;

    public GroupButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        this.group = new GroupCommand(this.drawingPane);
        command = drawingPane.history();
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            command.exec(group);
        } catch (Exception e){
            drawingPane.error(e.getMessage());
        }
    }
}
