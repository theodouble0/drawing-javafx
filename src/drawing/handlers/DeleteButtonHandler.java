package drawing.handlers;

import drawing.commands.CommandHistory;
import drawing.commands.DeleteCommand;
import drawing.commands.ICommand;
import drawing.ui.DrawingPane;
import drawing.shapes.IShape;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class DeleteButtonHandler implements EventHandler<ActionEvent> {

    private DrawingPane drawingPane;
    CommandHistory command;
    ICommand delete;

    public DeleteButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        delete = new DeleteCommand(drawingPane);
        command = drawingPane.history();
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            this.command.exec(delete);
        } catch (Exception e){
            drawingPane.error(e.getMessage());
        }
    }
}
