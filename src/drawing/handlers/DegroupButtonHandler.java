package drawing.handlers;

import drawing.commands.CommandHistory;
import drawing.commands.DegroupCommand;
import drawing.commands.GroupCommand;
import drawing.commands.ICommand;
import drawing.shapes.Composite;
import drawing.shapes.IShape;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.ArrayList;
import java.util.Iterator;

public class DegroupButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;
    CommandHistory command;
    ICommand degroup;

    public DegroupButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        this.degroup = new DegroupCommand(this.drawingPane);
        command = drawingPane.history();
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            command.exec(degroup);
        } catch (Exception e){
            drawingPane.error(e.getMessage());
        }
    }
}
