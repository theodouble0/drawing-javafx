package drawing.handlers;

import drawing.commands.ClearCommand;
import drawing.commands.CloneCommand;
import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class CloneButtonHandler  implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;
    CommandHistory command;

    public CloneButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        command = drawingPane.history();
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            this.command.exec(new CloneCommand(this.drawingPane));
        } catch (Exception e){
            drawingPane.error(e.getMessage());
        }
    }
}
