package drawing.handlers;

import drawing.commands.ClearCommand;
import drawing.commands.CommandHistory;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class UndoButtonHandler implements EventHandler<ActionEvent> {
    private CommandHistory history;
    private DrawingPane drawingPane;

    public UndoButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        this.history = drawingPane.history();
    }
    @Override
    public void handle(ActionEvent event) {
        try {
            history.undo();
        }catch (Exception e){
            drawingPane.error(e.getMessage());
        }

    }
}
