package drawing.handlers;

import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.commands.MoveCommand;
import drawing.ui.DrawingPane;
import drawing.shapes.IShape;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.awt.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by lewandowski on 20/12/2017.
 */
public class MouseMoveHandler implements EventHandler<MouseEvent> {

    private DrawingPane drawingPane;

    private double orgSceneX;
    private double orgSceneY;

    private double originX;
    private double originY;

    CommandHistory history;

    public MouseMoveHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        drawingPane.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        drawingPane.addEventHandler(MouseEvent.MOUSE_DRAGGED, this);
        drawingPane.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
        this.history = drawingPane.history();
    }

    @Override
    public void handle(MouseEvent event) {
        int size = 0;
        for (IShape s : drawingPane.getSelection()){
            size = size +1;
        }
        boolean selected = size != 0;
        //System.out.println(selected);
        if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
            orgSceneX = event.getSceneX();
            orgSceneY = event.getSceneY();
            originX = event.getSceneX();
            originY = event.getSceneY();
        }

        if (event.getEventType().equals(MouseEvent.MOUSE_DRAGGED) && selected) {
            double offsetX = event.getSceneX() - orgSceneX;
            double offsetY = event.getSceneY() - orgSceneY;

            for (IShape shape : drawingPane.getSelection()){
                shape.offset(offsetX, offsetY);
            }

            orgSceneX = event.getSceneX();
            orgSceneY = event.getSceneY();
        }
        if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED) && selected){
            double offsetX = orgSceneX - originX;
            double offsetY = orgSceneY - originY;
            try{
                this.drawingPane.history().exec(new MoveCommand(this.drawingPane, offsetX, offsetY));
            } catch (Exception e){
                drawingPane.error(e.getMessage());
            }
        }
    }
}