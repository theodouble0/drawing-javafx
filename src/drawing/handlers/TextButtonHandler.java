package drawing.handlers;

import drawing.commands.CloneCommand;
import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.commands.TextCommand;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class TextButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;
    CommandHistory command;
    ICommand text;

    public TextButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        this.text = new TextCommand(this.drawingPane);
        command = drawingPane.history();
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            this.command.exec(text);
        } catch (Exception e){
            System.out.println(e.getMessage());
            drawingPane.error(e.getMessage());
        }
    }

}
