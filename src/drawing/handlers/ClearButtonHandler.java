package drawing.handlers;

import drawing.commands.ClearCommand;
import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ClearButtonHandler implements EventHandler<ActionEvent> {

    private DrawingPane drawingPane;
    CommandHistory command;
    ICommand clear;

    public ClearButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        this.clear = new ClearCommand(this.drawingPane);
        command = drawingPane.history();
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            this.command.exec(clear);
        } catch (Exception e){
            drawingPane.error(e.getMessage());
        }
    }
}
