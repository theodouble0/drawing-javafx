package drawing.handlers;

import drawing.commands.CommandHistory;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javax.swing.*;

public class RedoButtonHandler implements EventHandler<ActionEvent> {
    private CommandHistory history;
    private DrawingPane drawingPane;

    public RedoButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        this.history = drawingPane.history();
    }
    @Override
    public void handle(ActionEvent event) {
        try{
        history.redo();
        }catch (Exception e){
            drawingPane.error(e.getMessage());
        }
    }
}
